import pkg from './package.json';
import hooks from './hooks';

// let base/subfolder be the package name, e.g. de-prototypes
const routerBase = '/' + pkg.name;

module.exports = {
  ssr: false,

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },

  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~plugins/axiosCorsInterceptor.ts', '~plugins/deFrameworkInit.ts'],

  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxtjs/style-resources', '@nuxtjs/axios'],

  /*
   ** Define subfolder route for deployment
   */
  router: {
    base: routerBase
  },

  /*
   ** Define a redirect hook to redirect the 'run dev' localhost root to the router.base path
   */
  hooks: hooks(routerBase),

  buildModules: [
    ['@nuxt/typescript-build', {
      typeCheck: true,
      ignoreNotFoundWarning: true
    }],
    ['@nuxtjs/vuetify', {
      theme: {
        dark: true,
        themes: {
          dark: { primary: '#43A047' }
        }
      }
    }
    ]
  ],

  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map'
      }
    }
  },

  css: ['~/assets/css/responsive-svg.css'],

};
