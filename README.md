## dasf-app-template

This project contains an application template for the data analytics software framework (dasf).


### Service Desk

For everyone without a Geomar Gitlab account, we setup the Service Desk feature for this repository.
It lets you communicate with the developers via a repository specific eMail address. Each request will be tracked via the Gitlab issuse tracker.

eMail: [gitlab+digital-earth-dasf-dasf-app-template-1990-issue-@git-issues.geomar.de](mailto:gitlab+digital-earth-dasf-dasf-app-template-1990-issue-@git-issues.geomar.de)


### dasf web components
see: https://git.geomar.de/digital-earth/dasf/dasf-web

### build and deploy
The dependencies are managed by npm

1. install nodejs (if not already) and update npm, e.g.

```bash
# Using Ubuntu
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs

# Update npm
npm install -g npm@latest
```

2. clone the de-app-template repository

3. cd into the `dasf-app-template` folder and install dependencies via

```bash
npm install
```

The `dasf-web` dependency is installed directly from npm. 
The dependency import can be changed to `../dasf-web`
in case it's cloned into the same parent folder as the project referencing it.

4. build deployable package via

```bash
npm run build
```

The built files can be found in the `dist` folder. Simply copy the contents to
a webserver to deploy them.

5. alternatively, for local development and testing the web application can
be run in development mode via
```bash
npm run dev
```
### Recommended Software Citation

`Eggert, Daniel; Dransch, Doris (2021): DASF: A data analytics software framework for distributed environments. GFZ Data Services. https://doi.org/10.5880/GFZ.1.4.2021.004`


### License
```
Copyright 2021 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Potsdam, Germany / DASF Data Analytics Software Framework

Licensed under the Apache License, Version 2.0 (the "License");
you may not use these files except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

### Contact
Dr.-Ing. Daniel Eggert  
eMail: <daniel.eggert@gfz-potsdam.de>


Helmholtz Centre Potsdam GFZ German Research Centre for Geoscienes  
Section 1.4 Remote Sensing & Geoinformatics  
Telegrafenberg  
14473 Potsdam  
Germany  


