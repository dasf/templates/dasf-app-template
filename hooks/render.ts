import redirectRootToPortal from './route-redirect-portal'

export default routerBase => {
    return {
        /**
         * 'render:setupMiddleware'
         * {@link node_modules/nuxt/lib/core/renderer.js}
         */
        setupMiddleware(app) {
            app.use('/', redirectRootToPortal(routerBase))
        }
    }
}