import render from './render'

export default routerBase => ({
  render: render(routerBase)
})