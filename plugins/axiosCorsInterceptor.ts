import axios from 'axios';

console.log("setting up axios request interceptor for CORS proxy injection")

declare global {
    interface Window {
      PROXY_URL: string
    }
}

// export const PROXY_URL = 'http://geoms.gfz-potsdam.de:88/';
window.PROXY_URL = 'http://rz-vm154.gfz-potsdam.de:8081/';
// export const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';


// Add the CORS request interceptor to prepend the reverse proxy url
axios.interceptors.request.use(function (config) {

    if (config && config.url && config.url.startsWith('http')) {
        // absolute url - prepend proxy url to avoid CORS errors
        config.url = window.PROXY_URL + config.url;
        config.headers['X-Requested-With'] = 'XMLHttpRequest';
    }

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});