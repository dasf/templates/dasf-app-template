import PulsarConnection from 'dasf-web/lib/messaging/PulsarConnection'
import { PulsarModuleResponse, PulsarModuleRequest, PulsarModuleRequestReceipt } from 'dasf-web/lib/messaging//PulsarMessages'
import { DefaultPulsarUrlBuilder } from 'dasf-web/lib/messaging//PulsarUrlBuilder'
import TemporalImageLayer from 'dasf-web/lib/map/model/TemporalImageLayer'
import NetcdfRasterSource from 'dasf-web/lib/map/model/NetcdfRasterSource'
import b64a from 'base64-arraybuffer'

export default class SpatioTemporalClient {
  private pulsarConnection: PulsarConnection

  public constructor () {
    this.pulsarConnection = new PulsarConnection(new DefaultPulsarUrlBuilder('localhost', '8080', 'default', 'spatio-temporal'))
  }

  private createGetDataRequest (): PulsarModuleRequest {
    const request = PulsarModuleRequest.createRequestMessage()
    request.payload = btoa(JSON.stringify({ func_name: 'get_data' }))

    return request
  }

  public getData (): Promise<TemporalImageLayer> {
    return new Promise((resolve: (value: TemporalImageLayer) => void, reject: (reason: string) => void) => {
      this.pulsarConnection.sendRequest(this.createGetDataRequest(),
        (response: PulsarModuleResponse) => {
          if (response.properties.status === 'success') {
            // parse the payload
            const netcdfB64 = JSON.parse(atob(response.payload))
            // convert the b64 into an arraybuffer and parse the buffer into an ol.Source object
            NetcdfRasterSource.create({
              data: b64a.decode(netcdfB64)
            }).then((src: NetcdfRasterSource) => {
              // wrap ntcdf source into a layer
              const imageLayer = new TemporalImageLayer({
                title: 'spatio-temporal data',
                source: src
              })

              resolve(imageLayer)
            })
          } else {
            reject(atob(response.payload))
          }
        },
        null,
        (receipt: PulsarModuleRequestReceipt) => {
          reject(receipt.errorMsg)
        })
    })
  }
}
