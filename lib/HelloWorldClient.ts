import PulsarConnection from 'dasf-web/lib/messaging/PulsarConnection'
import { PulsarModuleResponse, PulsarModuleRequest, PulsarModuleRequestReceipt } from 'dasf-web/lib/messaging//PulsarMessages'
import { DefaultPulsarUrlBuilder } from 'dasf-web/lib/messaging//PulsarUrlBuilder'

export default class HelloWorldClient {
  private pulsarConnection: PulsarConnection

  public constructor () {
    this.pulsarConnection = new PulsarConnection(new DefaultPulsarUrlBuilder('localhost', '8080', 'default', 'hello-world-topic'))
  }

  private createHelloWorldRequest (): PulsarModuleRequest {
    const request = PulsarModuleRequest.createRequestMessage()
    request.payload = btoa(JSON.stringify({ func_name: 'hello_world' }))

    return request
  }

  public helloWorld (): Promise<string> {
    return new Promise((resolve: (value: string) => void, reject: (reason: string) => void) => {
      this.pulsarConnection.sendRequest(this.createHelloWorldRequest(),
        (response: PulsarModuleResponse) => {
          if (response.properties.status === 'success') {
            resolve(JSON.parse(atob(response.payload)))
          } else {
            reject(atob(response.payload))
          }
        },
        null,
        (receipt: PulsarModuleRequestReceipt) => {
          reject(receipt.errorMsg)
        })
    })
  }
}
